"""I/O functions for Paramter Balancing SBtab files."""
# The MIT License (MIT)
#
# Copyright (c) 2022 Elad Noor, Weizmann Institute of Science
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import re
import warnings
from typing import Dict, Iterable, Iterator, Optional, Tuple

import numpy as np
import pandas as pd
import pkg_resources
from sbtab.SBtab import SBtabDocument, SBtabError, SBtabTable, read_csv

from . import RT, logger, ureg


VARIABLE_NAMES = [
    "standard chemical potential",  # (μ'0)
    "catalytic rate constant geometric mean",  # (ln k^V)
    "Michaelis constant",  # (ln k^M)
    # "activation constant",  # (ln k^A)
    # "inhibitory constant",  # (ln k^I)
    "concentration of enzyme",  # (ln u)
    "concentration",  # (ln c)
    "equilibrium constant",  # (ln k^eq)
    "substrate catalytic rate constant",  # (ln k^cat+)
    "product catalytic rate constant",  # (ln k^cat-)
    "forward maximal velocity",  # (ln v^max+)
    "reverse maximal velocity",  # (ln v^max-)
    "chemical potential",  # (μ')
    # "reaction affinity",
]

INDEPENDENT_VARIABLE_NAMES = [
    "standard chemical potential",  # (μ'0)
    "catalytic rate constant geometric mean",  # (ln k^V)
    "Michaelis constant",  # (ln k^M)
    # "activation constant",  # (ln k^A)
    # "inhibitory constant",  # (ln k^I)
    "concentration of enzyme",  # (ln u)
    "concentration",  # (ln c)
]

SPECIES_PARAMETERS = {
    "standard chemical potential",
    "concentration",
    "chemical potential",
}

REACTION_PARAMETERS = {
    "catalytic rate constant geometric mean",
    "equilibrium constant",
    "substrate catalytic rate constant",
    "product catalytic rate constant",
    "forward maximal velocity",
    "reverse maximal velocity",
    "concentration of enzyme",
}


class PriorData(object):
    """Parses the prior SBtab and stores the data."""

    DEFAULT_SBTAB_PATH = "data/pb_prior.tsv"

    PSEUDO_LIST = [
        "chemical potential",
        "product catalytic rate constant",
        "substrate catalytic rate constant",
        "equilibrium constant",
        "forward maximal velocity",
        "reverse maximal velocity",
        "reaction affinity",
    ]

    REQUIRED_COLUMNS = [
        "QuantityType",
        "Unit",
        "MathematicalType",
        "PriorMedian",
        "PriorStd",
        "PriorGeometricStd",
        "DataStd",
        "DataGeometricStd",
        "Dependence",
        "UseAsPriorInformation",
        "MatrixInfo",
    ]

    def __init__(self, sbtab: Optional[SBtabTable] = None) -> None:
        self.additives = set()
        self.multiplicatives = set()
        self.thermodynamic = set()
        self.priors = dict()
        self.bounds = dict()
        self.units = dict()

        if sbtab is None:
            # if a Prior table is not provided, read the default Prior
            # configuration file instead
            prior_path = pkg_resources.resource_filename(
                "pbalancing", PriorData.DEFAULT_SBTAB_PATH
            )
            try:
                sbtab = read_csv(prior_path, "prior.tsv").get_sbtab_by_id("Prior")
            except SBtabError as e:
                logger.error(f"The prior file ({prior_path}) could not be read.")
                raise e
        if not self.validate_sbtab(sbtab):
            raise ValueError("The input Prior SBtab is not valid")
        self.read_parameter_information(sbtab)
        # self.extract_pseudos(sbtab)

    def validate_sbtab(self, sbtab: SBtabTable) -> bool:
        """
        if the given SBtab file is a prior for parameter balancing, it needs to be
        checked thorougly for the validity of several features
        """
        valid = True

        # check table type
        if sbtab.table_type != "QuantityInfo":
            warnings.warn(
                "The TableType of the prior file is not correct: "
                f"{sbtab.table_type}. It should be 'QuantityInfo'"
            )
            valid = False

        prior_df = sbtab.to_data_frame()

        # check for required columns
        for col in set(PriorData.REQUIRED_COLUMNS).difference(prior_df.columns):
            warnings.warn(f"The crucial column {col} is missing in the prior file")
            valid = False

        # check for required row entries
        for qtype in set(VARIABLE_NAMES).difference(prior_df.QuantityType):
            warnings.warn(f"The crucial entry {qtype} is missing in the prior file")
            valid = False

        return valid

    def read_parameter_information(self, sbtab: SBtabTable) -> None:
        """Read a table file from the resources directory.

        The file holds information on the handling of the parameters,
        the parameter details, and how to build the dependency matrix
        for the different parameter types.
        """
        prior_df = sbtab.to_data_frame()

        # store only the prior data's mean and std (in log-scale when necessary)
        for row in prior_df.itertuples():
            qtype = row.QuantityType
            unit = row.Unit
            med = float(row.PriorMedian)
            lb = float(row.LowerBound)
            ub = float(row.UpperBound)

            if row.PhysicalType == "Thermodynamic":
                self.thermodynamic.add(qtype)
            if row.MathematicalType == "Additive":
                std = float(row.PriorStd)
                self.additives.add(qtype)
            elif row.MathematicalType == "Multiplicative":
                std = np.log(float(row.PriorGeometricStd))
                self.multiplicatives.add(qtype)
            else:
                raise ValueError(f"{qtype} is neither additive nor multiplicative")
            self.units[qtype] = ureg.Unit(unit)
            self.bounds[qtype] = (ureg.Quantity(lb, unit), ureg.Quantity(ub, unit))
            self.priors[qtype] = ureg.Quantity(med, unit).plus_minus(std)

    def get_prior(self, qtype: str) -> ureg.Measurement:
        return self.priors[qtype]

    def get_default_unit(self, qtype: str) -> ureg.Unit:
        return self.units[qtype]


class ModelData(object):

    POSSIBLE_REACTION_ARROWS = (
        # Three-character arrows.
        "<=>",
        "<->",
        "-->",
        "<--",
        # Two-character arrows.
        "=>",
        "<=",
        "->",
        "<-",
        # Single character unicode arrows.
        "=",
        "⇌",
        "⇀",
        "⇋",
        "↽",
    )

    def __init__(self, sbtabdoc: SBtabDocument) -> None:
        self.configuration = None
        self.standard_concentration = None
        self.water_cid = None
        self.proton_cid = None

        config_sbtab = sbtabdoc.get_sbtab_by_id("OptionsPB")
        self.read_configuration(config_sbtab)

        prior_sbtab = sbtabdoc.get_sbtab_by_id("Prior")
        self.prior_data = PriorData(prior_sbtab)

        reaction_sbtab = sbtabdoc.get_sbtab_by_id("Reaction")
        if reaction_sbtab is None:
            raise ValueError("Missing 'Reaction' table in SBtab file")
        self.read_reactions(reaction_sbtab)

        param_sbtab = sbtabdoc.get_sbtab_by_id("Parameter")
        if param_sbtab is None:
            raise ValueError("Missing 'Parameter' table in SBtab file")
        self.parameter_df = self.read_data(param_sbtab)

    def read_configuration(self, sbtab: SBtabTable) -> None:
        """Read the configuration table (tableID = OptionsPB)."""
        if sbtab is None:
            self.configuration = dict()
        else:
            config_df = sbtab.to_data_frame()
            self.configuration = config_df.set_index("Option").Value.to_dict()

        # read the 'standard concentration' from the sbtab, which is the
        # reference concentration used for thermodynamic calculations
        # (Bob Alberty and colleagues usually set it to 1 M, but 1 mM
        # is sometimes used especially as an in-vivo physiological standard).
        # This is important for exchanging equilibrium constants or standard
        # Gibbs energies between models.
        self.standard_concentration = ureg.Quantity(
            self.configuration.get("standard concentration", "1M")
        )

        self.water_cid = self.configuration.get("water", "h2o")
        self.proton_cid = self.configuration.get("proton", "h")

    def get_prior_unit(self, qtype) -> ureg.Unit:
        """Get the units of the prior for this variable."""
        return self.prior_data.get_default_unit(qtype)

    def get_prior_bounds(self, qtype) -> Tuple[ureg.Quantity, ureg.Quantity]:
        """Get the prior lower and upper bounds."""
        return self.prior_data.bounds[qtype]

    def get_prior(self, qtype) -> ureg.Measurement:
        """Get the prior value (mean and std)."""
        return self.prior_data.get_prior(qtype)

    @staticmethod
    def parse_formula_side(s: str) -> Dict[str, float]:
        """Parse one side of the reaction formula."""
        if s.strip() == "null":
            return {}

        compound_bag = {}
        for member in re.split(r"\s+\+\s+", s):
            tokens = member.split(None, 1)
            if len(tokens) == 0:
                continue
            if len(tokens) == 1:
                amount = 1.0
                compound = member
            else:
                try:
                    amount = float(tokens[0])
                except ValueError as e:
                    logger.error(f"Non-specific reaction: {s}")
                    raise e
                compound = tokens[1]

            compound_bag[compound] = compound_bag.get(compound, 0.0) + amount

        return compound_bag

    @staticmethod
    def parse_formula(formula: str) -> Dict[str, float]:
        """Parse a two-sided reaction formula."""
        tokens = []
        arrow = None
        for arrow in ModelData.POSSIBLE_REACTION_ARROWS:
            if formula.find(arrow) != -1:
                tokens = formula.split(arrow, 2)
                break

        if len(tokens) < 2:
            raise ValueError(
                f"Reaction does not contain an allowed arrow sign ({arrow}): "
                f" {formula}"
            )

        left = tokens[0].strip()
        right = tokens[1].strip()

        sparse_reaction = {}
        left_dict = ModelData.parse_formula_side(left)
        right_dict = ModelData.parse_formula_side(right)
        for cid, count in left_dict.items():
            sparse_reaction[cid] = sparse_reaction.get(cid, 0) - count

        for cid, count in right_dict.items():
            sparse_reaction[cid] = sparse_reaction.get(cid, 0) + count

        # remove compounds that are balanced out in the reaction,
        # i.e. their coefficient is 0
        return {k: v for k, v in sparse_reaction.items() if v != 0}

    def create_stoichiometric_matrix_from_reactions(
        self,
        reactions: Iterable[Dict[str, float]],
    ) -> pd.DataFrame:
        """Build a stoichiometric matrix.

        Parameters
        ----------
        reactions : Iterable[Dict[str, float]]
            The collection of reactions to build a stoichiometric
            matrix from.

        Returns
        -------
        DataFrame
            The stoichiometric matrix as a DataFrame whose indexes are the
            compounds and its columns are the reactions (in the same order as
            the input).

        """
        # Now get rid of the protons, since we are applying Alberty's
        # framework where their potential is set to 0, and the pH is held
        # as a controlled parameter.
        # likewise, water
        compounds = sorted(
            {
                c
                for rxn in reactions
                for c in rxn.keys()
                if c not in {self.water_cid, self.proton_cid}
            }
        )

        sparse_reactions = []
        for rxn in reactions:
            sparse = pd.Series(index=compounds, dtype=float)
            for met, coeff in rxn.items():
                if met not in {self.water_cid, self.proton_cid}:
                    sparse[met] = coeff
            sparse_reactions.append(sparse)
        stoichiometry = pd.concat(sparse_reactions, axis=1).fillna(0.0)
        return stoichiometry.astype(pd.SparseDtype(float, fill_value=0.0))

    def read_reactions(self, sbtab: SBtabTable) -> None:
        """Read the stoichiometric information (tableID = Reaction)."""
        reaction_df = sbtab.to_data_frame()
        reactions = reaction_df.ReactionFormula.apply(ModelData.parse_formula)
        self.stoichiometry = self.create_stoichiometric_matrix_from_reactions(reactions)
        self.stoichiometry.columns = reaction_df["ID"].str.strip()

    def read_data(self, sbtab: SBtabTable) -> pd.DataFrame:
        """Read the measured data from the SBtab (tableID = Parameter)."""
        parameter_df = sbtab.to_data_frame()

        data = []
        for row in parameter_df.itertuples():
            try:
                prior = self.get_prior(row.QuantityType)
                default_unit = self.get_prior_unit(row.QuantityType)
                assert (
                    ureg.Unit(row.Unit).dimensionality == default_unit.dimensionality
                ), f"wrong unit dimensionality ({row.Unit} != {default_unit})"

                med = None
                std = None

                lb, ub = self.get_prior_bounds(row.QuantityType)
                if row.Min != "":
                    min = ureg.Quantity(float(row.Min), row.Unit)
                else:
                    min = lb

                if row.Max != "":
                    max = ureg.Quantity(float(row.Max), row.Unit)
                else:
                    max = ub

                if row.Median != "":
                    med = ureg.Quantity(float(row.Median), row.Unit)
                    if min is not None and med < min:
                        med = min
                    if max is not None and med > max:
                        med = max
                    if row.QuantityType in self.prior_data.additives:
                        if row.Std != "":
                            std = float(row.Std)
                        else:
                            std = prior.m_as(default_unit).nominal_value
                        med = med.m_as(default_unit)
                    elif row.QuantityType in self.prior_data.multiplicatives:
                        if row.GeometricStd != "":
                            gstd = float(row.GeometricStd)
                            std = np.log(gstd)
                        else:
                            std = prior.m_as(default_unit).std_dev
                        med = np.log(med.m_as(default_unit))
                    else:
                        raise ValueError(
                            f"{row.QuantityType} is neither additive nor "
                            f"multiplicative"
                        )

                if row.QuantityType in SPECIES_PARAMETERS:
                    assert (
                        row.Compound
                    ), f"missing compound identifier for {row.QuantityType}"
                    idx = row.Compound
                elif row.QuantityType in REACTION_PARAMETERS:
                    assert (
                        row.Reaction
                    ), f"missing reaction identifier for {row.QuantityType}"
                    idx = row.Reaction
                else:
                    assert (
                        row.Compound
                    ), f"missing compound identifier for {row.QuantityType}"
                    assert (
                        row.Reaction
                    ), f"missing reaction identifier for {row.QuantityType}"
                    idx = (row.Compound, row.Reaction)

                # For thermodynamic variables, we need to add a unique step
                # which adjusts the values based on the standard
                # concentration. Although Keq is unitless, for reactions with an
                # unequal number of substrates and products the value of Keq
                # varies (e.g. if the standard concentration is 1mM or 1M).
                # If the Km values are given in mM, but the Gibbs energies
                # have a standard of 1M, using them as is in the Haldane
                # relationship would be wrong.
                if row.QuantityType in self.prior_data.thermodynamic:
                    km_units = self.get_prior_unit("Michaelis constant")
                    if self.standard_concentration != km_units:
                        s_imbalance = self.stoichiometry.loc[:, row.Reaction].sum(0)
                        ratio = float(km_units / self.standard_concentration)
                        if default_unit.dimensionless:
                            med += np.log(ratio) * s_imbalance
                        else:
                            med += RT * np.log(ratio) * s_imbalance

                data.append((row.QuantityType, idx, med, std, min, max))

            except AssertionError as e:
                warnings.warn(f"Syntax error in Parameter table, row {row}")
                raise e

        return pd.DataFrame(
            data=data, columns=["qtype", "id", "value", "std", "min", "max"]
        )

    def get_param_df(self, qtype: str) -> pd.DataFrame:
        return self.parameter_df.loc[self.parameter_df.qtype == qtype, :].set_index(
            "id"
        )

    def get_stochiometry_sparse(self) -> Iterator[Tuple[str, str, float]]:
        for rid in self.stoichiometry.columns:
            for cid in self.stoichiometry.index:
                coeff = self.stoichiometry.at[cid, rid]
                if coeff != 0:
                    yield coeff, cid, rid
