from typing import Tuple

import numpy as np
import pandas as pd
import scipy.linalg
import scipy.sparse

from . import RT
from .io import (
    INDEPENDENT_VARIABLE_NAMES,
    REACTION_PARAMETERS,
    SPECIES_PARAMETERS,
    VARIABLE_NAMES,
    ModelData,
)


class NewBalancer(object):
    def __init__(self, model: ModelData) -> None:
        self.model = model

    def get_stoichiometric_matrix(self) -> np.ndarray:
        return self.model.stoichiometry.values

    def get_auxiliary_matrix(self) -> np.ndarray:
        """Get the auxiliary matrix with Michaelis-Menten coefficients."""

        S = self.model.stoichiometry

        reactions = S.columns.tolist()
        S_sparse = list(self.model.get_stochiometry_sparse())
        pairs = [(cid, rid) for _, cid, rid in S_sparse]

        # create the N_kM auxiliary matrix, which is essentially a sparser
        # version of the stoichiometric matrix
        N_kM = pd.DataFrame(index=reactions, columns=pairs, dtype=float)
        for coeff, cid, rid in S_sparse:
            N_kM[(cid, rid)][rid] = coeff

        return N_kM.fillna(0.0).astype(pd.SparseDtype(float, fill_value=0.0))

    def get_dependence_matrix(self) -> pd.DataFrame:
        """Build the Q matrix that is needed for parameter balancing."""
        S = self.model.stoichiometry.sparse.to_coo()
        N_kM = self.get_auxiliary_matrix().sparse.to_coo()
        n_c, n_r = S.shape
        n_k = N_kM.shape[1]  # the number of K_M values
        # n_a = self.model.get_param_df("activation constant").shape[0]
        # n_i = self.model.get_param_df("inhibitory constant").shape[0]

        # block sizes
        block_sizes = {
            "standard chemical potential": n_c,
            "catalytic rate constant geometric mean": n_r,
            "Michaelis constant": n_k,
            # "activation constant": n_a,
            # "inhibitory constant": n_i,
            "concentration of enzyme": n_r,
            "concentration": n_c,
            "equilibrium constant": n_r,
            "substrate catalytic rate constant": n_r,
            "product catalytic rate constant": n_r,
            "forward maximal velocity": n_r,
            "reverse maximal velocity": n_r,
            "chemical potential": n_c,
        }

        I_c = scipy.sparse.coo_matrix(np.eye(n_c))
        I_r = scipy.sparse.coo_matrix(np.eye(n_r))
        I_k = scipy.sparse.coo_matrix(np.eye(n_k))

        # non-zero blocks
        dependency_blocks = {
            ("standard chemical potential", "standard chemical potential"): I_c,
            ("standard chemical potential", "equilibrium constant"): -S.T / RT,
            ("standard chemical potential", "substrate catalytic rate constant"): -S.T
            / (2 * RT),
            ("standard chemical potential", "product catalytic rate constant"): S.T
            / (2 * RT),
            ("standard chemical potential", "forward maximal velocity"): -S.T
            / (2 * RT),
            ("standard chemical potential", "reverse maximal velocity"): S.T / (2 * RT),
            ("standard chemical potential", "chemical potential"): I_c,
            (
                "catalytic rate constant geometric mean",
                "catalytic rate constant geometric mean",
            ): I_r,
            (
                "catalytic rate constant geometric mean",
                "substrate catalytic rate constant",
            ): I_r,
            (
                "catalytic rate constant geometric mean",
                "product catalytic rate constant",
            ): I_r,
            (
                "catalytic rate constant geometric mean",
                "forward maximal velocity",
            ): I_r,
            (
                "catalytic rate constant geometric mean",
                "reverse maximal velocity",
            ): I_r,
            (
                "Michaelis constant",
                "Michaelis constant",
            ): I_k,
            (
                "Michaelis constant",
                "substrate catalytic rate constant",
            ): -N_kM
            / 2.0,
            (
                "Michaelis constant",
                "product catalytic rate constant",
            ): N_kM
            / 2.0,
            (
                "Michaelis constant",
                "forward maximal velocity",
            ): -N_kM
            / 2.0,
            (
                "Michaelis constant",
                "reverse maximal velocity",
            ): N_kM
            / 2.0,
            # (
            #     "activation constant",
            #     "activation constant",
            # ): scipy.sparse.identity(n_a),
            # (
            #     "inhibitory constant",
            #     "inhibitory constant",
            # ): scipy.sparse.identity(n_i),
            (
                "concentration of enzyme",
                "concentration of enzyme",
            ): I_r,
            (
                "concentration of enzyme",
                "forward maximal velocity",
            ): I_r,
            (
                "concentration of enzyme",
                "reverse maximal velocity",
            ): I_r,
            (
                "concentration",
                "concentration",
            ): I_c,
            (
                "concentration",
                "chemical potential",
            ): I_c
            * RT,
        }

        column_ids = self.get_data_index(include_dependent=False)
        row_ids = self.get_data_index(include_dependent=True)
        Q_blocks = []
        for p2 in VARIABLE_NAMES:
            n2 = block_sizes[p2]
            row = []
            for p1 in INDEPENDENT_VARIABLE_NAMES:
                n1 = block_sizes[p1]
                B = dependency_blocks.get(
                    (p1, p2), scipy.sparse.coo_matrix((n2, n1), dtype=float)
                )
                assert B.shape == (n2, n1)
                row.append(B.todense())
            Q_blocks.append(row)
        return (
            pd.DataFrame(data=np.block(Q_blocks), columns=column_ids, index=row_ids)
            .fillna(0.0)
            .astype(pd.SparseDtype(float, fill_value=0.0))
        )

    def get_data_index(self, include_dependent: bool = True) -> pd.Index:
        """Get the indices used in the matrix notations (for Q, C, mu, etc.)

        independent (bool) - whether to only include
        """
        cids = self.model.stoichiometry.index
        rids = self.model.stoichiometry.columns
        pairs = [(cid, rid) for _, cid, rid in self.model.get_stochiometry_sparse()]

        qtypes = []
        ids = []

        for qtype in (
            VARIABLE_NAMES if include_dependent else INDEPENDENT_VARIABLE_NAMES
        ):
            if qtype in SPECIES_PARAMETERS:
                qtypes += [qtype] * len(cids)
                ids += cids.to_list()
            elif qtype in REACTION_PARAMETERS:
                qtypes += [qtype] * len(rids)
                ids += rids.to_list()
            elif qtype == "Michaelis constant":
                qtypes += [qtype] * len(pairs)
                ids += pairs
            else:
                raise KeyError(f"{qtype} is not yet supported as a data input")

        return pd.MultiIndex.from_arrays([qtypes, ids], names=["qtype", "id"])

    def get_data(self) -> Tuple[pd.DataFrame, pd.DataFrame]:
        """Get the mean and precision matrix of the data."""
        idx = self.get_data_index(include_dependent=True)

        # adjust the index of the data matrix to the full parameter list (i.e.
        # like the rows in Q). missing values will be filled with NaN.
        df = self.model.parameter_df.set_index(["qtype", "id"]).reindex(idx)

        mu = df["value"].fillna(0.0)
        precision = []
        for row in df.itertuples():
            if np.isnan(row.value) or np.isnan(row.std):
                precision.append(0.0)
            else:
                precision.append(1.0 / row.std**2)

        return mu, pd.DataFrame(data=np.diag(precision), index=idx, columns=idx).astype(
            pd.SparseDtype(float, fill_value=0.0)
        )

    def get_prior(self) -> Tuple[pd.DataFrame, pd.DataFrame]:
        """Get the mean and precision matrix of the prior."""
        idx = self.get_data_index(include_dependent=False)

        # generate the prior covariance matrix from the standard deviations
        qtypes = idx.to_frame().qtype.to_list()
        units = map(self.model.prior_data.get_default_unit, qtypes)
        priors = map(self.model.prior_data.get_prior, qtypes)
        priors = [p.m_as(u) for p, u in zip(priors, units)]

        means = [p.nominal_value for p in priors]
        stds = [p.std_dev for p in priors]
        return pd.DataFrame(data=means, index=idx), pd.DataFrame(
            data=np.diag([1.0 / s**2 for s in stds]), index=idx, columns=idx
        ).astype(pd.SparseDtype(float, fill_value=0.0))

    def balance(self) -> Tuple[pd.DataFrame, pd.DataFrame]:
        """Perform the parameter balancing itself."""
        # should we use the scipy.linalg.spsolve() function ?
        dependence = self.get_dependence_matrix()
        mean_prior, precision_prior = self.get_prior()
        mean_data, precision_data = self.get_data()

        Q = dependence.sparse.to_coo()
        mu_prior = mean_prior.values
        P_prior = precision_prior.sparse.to_coo()
        mu_data = mean_data.values
        P_data = precision_data.sparse.to_coo()

        P_post = P_prior + Q.T @ P_data @ Q

        b_data = (Q.T @ P_data @ mu_data).flatten()
        b_prior = (P_prior @ mu_prior).flatten()
        mu_post = scipy.sparse.linalg.spsolve(A=P_post, b=b_data + b_prior)

        mean_post = pd.DataFrame(
            data=mu_post,
            index=precision_prior.columns,
        )
        precision_post = pd.DataFrame(
            data=P_post.todense(),
            index=precision_prior.index,
            columns=precision_prior.columns,
        ).astype(pd.SparseDtype(float, fill_value=0.0))
        return mean_post, precision_post
