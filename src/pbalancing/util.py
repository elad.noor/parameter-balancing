# The MIT License (MIT)
#
# Copyright (c) 2018 Timo Lubitz
# Copyright (c) 2022 Elad Noor, Weizmann Institute of Science
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import copy
import random
import struct
from typing import List, Optional, Tuple, Union

import cvxpy
import numpy as np
import scipy
from sbtab.SBtab import SBtabDocument, SBtabTable, read_csv

from . import logger


DEFAULT_PARAMETERS = {
    "temperature": 300.0,
    "ph": 7.0,
    "standard chemical potential": True,
    "catalytic rate constant geometric mean": True,
    "Michaelis constant": True,
    "activation constant": True,
    "inhibitory constant": True,
    "concentration": True,
    "concentration of enzyme": True,
    "equilibrium constant": True,
    "substrate catalytic rate constant": True,
    "product catalytic rate constant": True,
    "forward maximal velocity": True,
    "reverse maximal velocity": True,
    "chemical potential": True,
    "reaction affinity": True,
    "use_pseudo_values": False,
}

TRANSFER_MODE_DICT = {
    "standard chemical potential": "weg",
    "equilibrium constant": "hal",
    "catalytic rate constant": "cat",
}


def open_sbtabdoc(filename: Union[str, SBtabDocument]) -> SBtabDocument:
    """Open a file as an SBtabDocument.

    Checks whether it is already an SBtabDocument object, otherwise reads the
    CSV file and returns the parsed object.
    """
    if isinstance(filename, str):
        return read_csv(filename, "pbalancing")
    elif isinstance(filename, SBtabDocument):
        return filename
    else:
        raise ValueError("'filename' argument must be string or SBtabDocument")


def id_checker(sbtab: SBtabTable, sbml):
    """
    this function checks, whether all the entries of the SBML ID columns of the
    SBtab file can also be found in the SBML file. If not, these are omitted
    during the balancing. But there should be a warning to raise user awareness.
    """
    sbtabid2sbmlid = []

    reaction_ids_sbml = []
    species_ids_sbml = []

    s_id = None
    r_id = None

    for reaction in sbml.getListOfReactions():
        reaction_ids_sbml.append(reaction.getId())
    for species in sbml.getListOfSpecies():
        species_ids_sbml.append(species.getId())

    for row in sbtab.value_rows:
        if len(row) < 3:
            continue
        try:
            s_id = sbtab.columns_dict["!Compound"]
        except Exception:
            try:
                s_id = sbtab.columns_dict["!Compound:SBML:species:id"]
            except Exception:
                sbtabid2sbmlid.append(
                    'Error: The SBtab file lacks the obligatory column "'
                    "!Compound"
                    '"/"'
                    "!Compound:SBML:species:id"
                    '" to link the parameter entries to the SBML model species.'
                )

        try:
            r_id = sbtab.columns_dict["!Reaction"]
        except Exception:
            try:
                r_id = sbtab.columns_dict["!Reaction:SBML:reaction:id"]
            except Exception:
                sbtabid2sbmlid.append(
                    'Error: The SBtab file lacks the obligatory column "'
                    "!Reaction"
                    '"/"'
                    "!Reaction:SBML:reaction:id"
                    '" to link the parameter entries to the SBML model species.'
                )
        try:
            if (
                row[s_id] != ""
                and row[s_id] not in species_ids_sbml
                and row[s_id] != "nan"
                and row[s_id] != "None"
            ):
                sbtabid2sbmlid.append(
                    "Warning: The SBtab file holds a species ID which does not comply to any species ID in the SBML file: %s"
                    % (row[s_id])
                )
        except Exception:
            pass
        try:
            if (
                row[r_id] != ""
                and row[r_id] not in reaction_ids_sbml
                and row[r_id] != "nan"
                and row[r_id] != "None"
            ):
                sbtabid2sbmlid.append(
                    "Warning: The SBtab file holds a reaction ID which does not comply to any reaction ID in the SBML file: %s"
                    % (row[r_id])
                )
        except Exception:
            pass

    return sbtabid2sbmlid


def valid_prior(sbtab_prior: SBtabTable) -> List[str]:
    """
    if the given SBtab file is a prior for parameter balancing, it needs to be
    checked thorougly for the validity of several features
    """
    validity = []

    # check table type
    if sbtab_prior.table_type != "QuantityInfo":
        validity.append(
            "Error: The TableType of the prior file is not correct: "
            f"{sbtab_prior.table_type}. It should be QuantityInfo."
        )

    # check for required columns
    required_columns = [
        "!QuantityType",
        "!Unit",
        "!MathematicalType",
        "!PriorMedian",
        "!PriorStd",
        "!PriorGeometricStd",
        "!DataStd",
        "!Dependence",
        "!UseAsPriorInformation",
        "!MatrixInfo",
    ]
    for column in required_columns:
        if column not in sbtab_prior.columns_dict:
            validity.append(
                "Error: The crucial column %s is missing in" " the prior file." % column
            )

    # check for required row entries
    required_rows = [
        "standard chemical potential",
        "catalytic rate constant geometric mean",
        "concentration",
        "concentration of enzyme",
        "Michaelis constant",
        "inhibitory constant",
        "activation constant",
        "chemical potential",
        "product catalytic rate constant",
        "substrate catalytic rate constant",
        "equilibrium constant",
        "forward maximal velocity",
        "reverse maximal velocity",
        "reaction affinity",
    ]
    for row in sbtab_prior.value_rows:
        try:
            required_rows.remove(row[sbtab_prior.columns_dict["!QuantityType"]])
        except Exception:
            pass

    for row in required_rows:
        validity.append(
            "Error: The prior file is missing an entry for th" "crucial value %s." % row
        )

    return validity


def extract_pseudos_priors(sbtab_prior: SBtabTable) -> Tuple[dict, dict, dict, dict]:
    """
    extracts the priors and pseudos of a given SBtab prior table
    """
    pseudo_list = [
        "chemical potential",
        "product catalytic rate constant",
        "substrate catalytic rate constant",
        "equilibrium constant",
        "forward maximal velocity",
        "reverse maximal velocity",
        "reaction affinity",
    ]
    pmin = {}
    pmax = {}
    pseudos = {}
    priors = {}

    for row in sbtab_prior.value_rows:
        pmin[row[sbtab_prior.columns_dict["!QuantityType"]]] = float(
            row[sbtab_prior.columns_dict["!LowerBound"]]
        )
        pmax[row[sbtab_prior.columns_dict["!QuantityType"]]] = float(
            row[sbtab_prior.columns_dict["!UpperBound"]]
        )
        if row[sbtab_prior.columns_dict["!MathematicalType"]] == "Additive":
            std = row[sbtab_prior.columns_dict["!PriorStd"]]
        else:
            std = row[sbtab_prior.columns_dict["!PriorGeometricStd"]]
        median = row[sbtab_prior.columns_dict["!PriorMedian"]]

        if row[sbtab_prior.columns_dict["!QuantityType"]] in pseudo_list:
            pseudos[row[sbtab_prior.columns_dict["!QuantityType"]]] = [
                float(median),
                float(std),
            ]
        else:
            priors[row[sbtab_prior.columns_dict["!QuantityType"]]] = [
                float(median),
                float(std),
            ]

    return pseudos, priors, pmin, pmax


def readout_config(sbtab_options):
    """
    reads out the content of an optional config file and returns a parameter
    dictionary with many options for the balancing process
    """
    parameter_dict = {"config": True}
    log = []
    allowed_options = [
        "use_pseudo_values",
        "ph",
        "temperature",
        "overwrite_kinetics",
        "cell_volume",
        "parametrisation",
        "enzyme_prefactor",
        "default_inhibition",
        "default_activation",
        "model_name",
        "boundary_values",
        "samples",
        "size_limit",
    ]

    if "!ID" not in sbtab_options.columns_dict:
        log.append(
            "Error: The crucial option (ID) column is missing from the" "options file"
        )
    if "!Value" not in sbtab_options.columns_dict:
        log.append("Error: The crucial value column is missing from the" "options file")

    for row in sbtab_options.value_rows:
        if row[sbtab_options.columns_dict["!ID"]] not in allowed_options:
            log.append(
                "There was an irregular option in the options file: "
                f"{row[sbtab_options.columns_dict['!ID']]}"
            )
        else:
            if row[sbtab_options.columns_dict["!Value"]] == "":
                log.append(
                    "There is no value set for option:"
                    "%s" % row[sbtab_options.columns_dict["!ID"]]
                )

        parameter_dict[row[sbtab_options.columns_dict["!ID"]]] = row[
            sbtab_options.columns_dict["!Value"]
        ]

    return parameter_dict, log


def get_modifiers(reaction):
    """
    get iterator for the reaction modifiers
    @type reaction: libsbml.Reaction
    @rtype: Iterator
    """
    for s in reaction.getListOfModifiers():
        yield s


def get_participants(reaction):
    """
    get iterator for the reaction participants (reactants + products)
    @type reaction: libsbml.Reaction
    @rtype: Iterator
    """
    for s in reaction.getListOfReactants():
        yield s
    for s in reaction.getListOfProducts():
        yield s


def is_enzyme(species):
    return (
        species.getSBOTerm() == 14
        or species.getId().startswith("enzyme")
        or species.getSBOTerm() == 460
    )


def get_enzyme_for_reaction(reaction, create=False):
    is_enzyme = (
        lambda s: s.getSBOTerm() == 14
        or s.getId().startswith("enzyme")
        or s.getSBOTerm() == 460
    )
    for m in reaction.getListOfModifiers():
        s = reaction.getModel().getSpecies(m.getSpecies())
        if is_enzyme(s):
            return s
    if create:
        e = reaction.getModel().createSpecies()
        e.setId("enzyme_" + reaction.getId())
        e.setName("enzyme_" + reaction.getId())
        try:
            comp = (
                reaction.getModel()
                .getSpecies(reaction.getReactant(0).getSpecies())
                .getCompartment()
            )
        except Exception:
            comp = (
                reaction.getModel()
                .getSpecies(reaction.getProduct(0).getSpecies())
                .getCompartment()
            )
        if not comp:
            comp = reaction.getModel().getCompartment(0).getId()
        e.setCompartment(comp)
        e.setSBOTerm(460)
        mod = reaction.createModifier()
        mod.setSpecies(e.getId())
        return e
    raise Exception("No enzyme was found for reaction %s" % reaction.getId())


def fmin_cvxpy(
    median: np.ndarray,
    cov: np.ndarray,
    bounds: Optional[List[Tuple[float, float]]] = None,
    variable_is_logarithmic: Optional[List[bool]] = None,
) -> Optional[np.ndarray]:
    """Use QP to find the mode of a Gaussian within a bounded space."""
    n = median.shape[0]
    assert cov.shape == (n, n)
    if bounds is None:
        bounds = [(1e-4, 1e4)] * n
    else:
        assert len(bounds) == n

    if variable_is_logarithmic is None:
        variable_is_logarithmic = [True] * n
    else:
        assert len(variable_is_logarithmic) == n

    x = cvxpy.Variable(shape=(n,), name="x")
    obj = cvxpy.Minimize(cvxpy.quad_form(x - median, np.linalg.inv(cov)))

    lbs = []
    ubs = []
    for is_log, (lb, ub) in zip(variable_is_logarithmic, bounds):
        lbs.append(np.log(lb) if is_log else lb)
        ubs.append(np.log(ub) if is_log else ub)
    constraints = [cvxpy.Constant(lbs) <= x, x <= cvxpy.Constant(ubs)]

    prob = cvxpy.Problem(objective=obj, constraints=constraints)
    prob.solve()
    if prob.status == "optimal":
        return x.value
    else:
        logger.error(
            f"Could not find the posterior mode, optimization "
            f"problem is : {prob.status}"
        )
        raise ValueError("Posterior mode problem couldn't be solved")


# TODO: remove fmin_gen(), which was replaced with fmin_cvxpy()
def fmin_gen(
    f_opt,
    x0: np.ndarray,
    medians: np.ndarray,
    C_post: np.ndarray,
    population_size: int = 100,
    survivors: int = 20,
    generations: int = 20000,
    bounds: Optional[List[Tuple[float, float]]] = None,
    variable_is_logarithmic: Optional[List[bool]] = None,
    intruders: int = 0,
) -> np.ndarray:
    """Generate optimization (deprecated)."""

    def local_optimize(indiv, convenience_class=None):
        better_indiv = indiv
        if convenience_class:
            better_indiv = scipy.optimize.fmin_bfgs(
                convenience_class.f, better_indiv, disp=0, maxiter=20
            )
            fval = convenience_class.f_opt(better_indiv, medians, C_post)
        else:
            fval = f_opt(better_indiv)
        return [better_indiv, fval]

    def float_to_bits(value: float) -> str:
        if type(value) == float:
            return (str(struct.unpack("Q", struct.pack("d", value))[0])).rjust(20, "0")
        else:
            return ",".join([float_to_bits(x) for x in value.tolist()])

    def bits_to_float(bits: str) -> float:
        if "," in bits:
            return np.array([bits_to_float(x) for x in bits.split(",")])
        else:
            return struct.unpack("d", struct.pack("Q", int(bits)))[0]

    def new_individual() -> np.ndarray:
        x = []
        for i in range(indiv_size):
            if variable_is_logarithmic[i]:
                logmin = np.log(bounds[i][0])
                logmax = np.log(bounds[i][1])
                x.append(np.exp(np.random.rand() * (logmax - logmin) + logmin))
            else:
                x.append(
                    np.random.rand() * (bounds[i][1] - bounds[i][0]) + bounds[i][0]
                )
        return np.array(x)

    def bool_mate(mother: float, father: float) -> float:
        ms = float_to_bits(mother)
        fs = float_to_bits(father)
        l = random.sample(range(len(ms)), 3)
        l.sort()
        [i1, i2, i3] = l
        cs = ms[:i1] + fs[i1:i2] + ms[i2:i3] + fs[i3:]
        child = bits_to_float(cs)
        if (
            child.size != mother.size
            or None in child
            or np.inf in child
            or -np.inf in child
        ):
            raise ValueError()
        return child

    def mate(mflist: list) -> float:
        return mflist[0] + mflist[1] - mflist[2]

    def mutate(indiv: float) -> float:
        if not False:
            bi = float_to_bits(indiv)
            number = int(np.ceil(float(len(bi)) / 100.0))
            change_indices = random.sample([x for x in range(3, len(bi))], number)
            for ci in change_indices:
                new = bi[:ci] + str(np.random.choice(range(10))) + bi[(ci + 1) :]
                try:
                    bits_to_float(new)
                    bi = new
                except struct.error:
                    # don't accept change
                    pass
            return np.abs(bits_to_float(bi))
        else:
            return np.exp(
                np.log(indiv)
                + wolf["mutation_factor"]
                * np.array([np.random.normalvariate(0, 1) for x in range(indiv_size)])
            )

    def bound(vector):
        global correct_vector

        if len(vector) == indiv_size:
            correct_vector = vector

        if len(vector) != indiv_size:
            vector = correct_vector

        for i in range(indiv_size):
            if vector[i] < bounds[i][0]:
                vector[i] = bounds[i][0]
                correct_vector = vector
            elif vector[i] > bounds[i][1]:
                vector[i] = bounds[i][1]
                correct_vector = vector
        return vector

    if bounds is None:
        bounds = [(1e-4, 1e4)] * len(x0)
    if len(bounds) != len(x0):
        raise ValueError("Length of x0 and length of bounds do not fit!")
    if variable_is_logarithmic is None:
        variable_is_logarithmic = [True] * len(x0)
    if len(variable_is_logarithmic) != len(x0):
        raise ValueError("Length of variable_is_logarithmic and x0 do not fit!")

    population = [x0]
    indiv_size = x0.size
    quality = []
    for i in range(population_size - len(population)):
        population.append(new_individual())
    best_indiv = copy.deepcopy(x0)

    try:
        for i in range(generations):
            # rate individuals
            local_optimization_results = []
            pre_computed_qualities = len(quality)

            for j in range(pre_computed_qualities, population_size):
                local_optimization_results.append(local_optimize(population[j]))

            for j in range(len(local_optimization_results)):
                [better_indiv, fval] = local_optimization_results[j]
                population[pre_computed_qualities + j] = better_indiv
                quality.append(fval)

            # replace None results
            for j in range(len(population)):
                while quality[j] is None or np.isnan(quality[j]):
                    population[j] = new_individual()
                    quality[j] = f_opt(population[j], medians, C_post)

            # sort
            sorted_quality = list(zip(quality, population))
            sorted_quality.sort(key=lambda x: x[0])
            new_population = []
            new_quality = []
            for j in range(survivors):
                f_val, indiv = sorted_quality[j]
                new_population.append(indiv)
                new_quality.append(f_val)
            population = new_population
            quality = new_quality

            # intrude
            for j in range(intruders):
                population.append(new_individual())

            # mate
            current_size = len(population)
            for j in range(current_size, population_size):
                population.append(
                    bound(mutate(mate(random.sample(population[:current_size], 3))))
                )
            best_indiv = copy.deepcopy(population[0])
            logger.debug(f"generation {str(i + 1):7s}     f = {quality[0]}")
            logger.debug(f"best_indiv = {best_indiv}")
    except KeyboardInterrupt:
        logger.info("Stopping computation")

    logger.debug(f"generation goodbye      f = {quality[0]}")

    return best_indiv
