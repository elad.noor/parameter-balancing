"""unit test for basic utility functions."""
# The MIT License (MIT)
#
# Copyright (c) 2022 Weizmann Institute of Science
# Copyright (c) 2022 INRAE
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import numpy as np
import pytest

from pbalancing import ureg
from pbalancing.io import ModelData, PriorData
from pbalancing.new_balancer import NewBalancer
from pbalancing.util import open_sbtabdoc


def test_prior():
    """Unit-test the prior data."""
    prior_data = PriorData()
    # assert prior_data.quantity2identifier["Michaelis constant"] == "kmc"
    assert prior_data.get_default_unit("Michaelis constant") == ureg.Unit("mM")


def test_sbtab(test_dir):
    """Unit-test the prior data."""
    sbtab_path = test_dir / f"ecoli_noor_2016.tsv"
    model_sbtabdoc = open_sbtabdoc(sbtab_path)

    model_data = ModelData(model_sbtabdoc)

    assert model_data.standard_concentration == ureg.Quantity(1, "M")
    # don't forget to check this - i.e. the conversion of thermodynamic
    # values given with a standard concentration different than the Km values
    # which are in mM

    assert "h2o" not in model_data.stoichiometry.index
    assert model_data.stoichiometry.at["g6p", "pts"] == pytest.approx(1.0)
    assert model_data.stoichiometry.at["glc", "pts"] == pytest.approx(-1.0)

    Km = model_data.get_param_df("Michaelis constant")
    assert Km.at[("g6p", "pts"), "value"] == pytest.approx(np.log(0.2), abs=1e-2)
    assert Km.at[("g6p", "pts"), "std"] == pytest.approx(np.log(10), abs=1e-2)

    # the value of this Km is 1050, but we expect it to be replaced by
    # the upper bound in the prior (1000)
    assert Km.at[("s7p", "tal"), "value"] == pytest.approx(np.log(1000), abs=1e-2)
    assert Km.at[("s7p", "tkt1"), "value"] == pytest.approx(np.log(1e-6), abs=1e-2)

    kcat = model_data.get_param_df("product catalytic rate constant")
    assert kcat.at["ppc", "value"] == pytest.approx(np.log(2315.0), rel=1e-2)
    assert kcat.at["ppc", "std"] == pytest.approx(np.log(100), rel=1e-2)

    balancer = NewBalancer(model_data)
    N_kM = balancer.get_auxiliary_matrix()
    assert N_kM.at["pts", ("g6p", "pts")] == 1
    assert N_kM.at["pts", ("glc", "pts")] == -1

    mean_post, precision_post = balancer.balance()
    assert mean_post.shape == (245, 1)
    assert precision_post.shape == (245, 245)
