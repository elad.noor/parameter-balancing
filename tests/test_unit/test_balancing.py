"""unit test for parameter balancing python API."""
# The MIT License (MIT)
#
# Copyright (c) 2022 Weizmann Institute of Science
# Copyright (c) 2022 INRAE
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import copy
import os
import sys

import libsbml
import pkg_resources
import pytest
from sbtab import validatorSBtab
from sbtab.SBtab import SBtabError, read_csv

from pbalancing import balancer, kineticizer, logger
from pbalancing.util import (
    DEFAULT_PARAMETERS,
    extract_pseudos_priors,
    id_checker,
    open_sbtabdoc,
    readout_config,
    valid_prior,
)


@pytest.mark.parametrize(
    "example_name", [("ecoli_carlsson_modified"), ("teusink"), ("hynne")]
)
def test_balancing(example_name, test_dir):
    """Unit-test balancing a model."""
    sbml_path = test_dir / f"{example_name}_model.xml"
    sbtab_path = test_dir / f"{example_name}.tsv"

    assert os.path.exists(sbml_path)
    assert os.path.exists(sbtab_path)

    # Test the SBML validity
    reader = libsbml.SBMLReader()
    sbml_file = reader.readSBML(sbml_path)
    assert sbml_file is not None, "readSBML() failed"

    sbml_model = sbml_file.getModel()
    assert sbml_model is not None, "getModel() failed"

    pb = balancer.ParameterBalancing(sbml_model)

    # Test the SBtab input file
    sbtabdoc = open_sbtabdoc(sbtab_path)

    # read the parameter data
    sbtab_data = sbtabdoc.get_sbtab_by_id("ParameterData")
    assert validatorSBtab.ValidateTable(sbtab_data).return_output() == []

    sbtab_prior = sbtabdoc.get_sbtab_by_id("Prior")
    if sbtab_prior is None:
        sbtab_prior = open_sbtabdoc(
            pkg_resources.resource_filename("pbalancing", "data/pb_prior.tsv")
        ).get_sbtab_by_id("Prior")
    assert valid_prior(sbtab_prior) == []
    pb.get_parameter_information(sbtab_prior)
    pseudos, priors, pmin, pmax = extract_pseudos_priors(sbtab_prior)

    sbtab_options = sbtabdoc.get_sbtab_by_id("ConfigurePB")
    if sbtab_options is None:
        sbtab_options = open_sbtabdoc(
            pkg_resources.resource_filename("pbalancing", "data/pb_options.tsv")
        ).get_sbtab_by_id("ConfigurePB")
    assert validatorSBtab.ValidateTable(sbtab_options).return_output() == []
    parameter_dict, log = readout_config(sbtab_options)
    assert log == []
    _parameter_dict = DEFAULT_PARAMETERS.copy()
    _parameter_dict.update(parameter_dict)
    parameter_dict = _parameter_dict

    sbtab = pb.make_sbtab(
        sbtab_data, sbtab_path, "All organisms", pmin, pmax, parameter_dict
    )
    assert id_checker(sbtab, sbml_model) == []

    sbtab_old = copy.deepcopy(sbtab)
    sbtab_new = pb.fill_sbtab(sbtab_old, pseudos, priors)

    (
        sbtab_final,
        mean_vector,
        mean_vector_inc,
        c_post,
        c_post_inc,
        r_matrix,
        shannon,
        concat_file,
    ) = pb.make_balancing(sbtab_new, sbtab, pmin, pmax, parameter_dict)

    #    with open(test_dir / f"{example_name}_result.tsv", "wt") as fp:
    #        fp.write(sbtab_final.to_str())
    #        return

    df_final = sbtab_final.to_data_frame()

    try:
        sbtab_ref = read_csv(
            test_dir / f"reference_{example_name}.tsv", "reference.tsv"
        ).get_sbtab_by_id("ParameterBalanced")
    except SBtabError as e:
        raise ValueError(
            f"The reference SBtab is not valid: '{sbtab_path}'.\n" + str(e)
        )

    df_ref = sbtab_ref.to_data_frame()

    for col in ["Mode", "UnconstrainedStd", "UnconstrainedMean"]:
        v_final = df_final[col].apply(float).values
        v_ref = df_ref[col].apply(float).values
        assert v_final == pytest.approx(v_ref, rel=1e-3)

    # Inserting parameters and kinetics into SBML model
    kineticizer_cs = kineticizer.KineticizerCS(
        model=sbml_model,
        sbtab=sbtab_final,
        mode="hal",
        enzyme_prefac=True,
        default_inh="complete_inh",
        default_act="complete_act",
        overwrite_existing=True,
        writer=sys.stderr,
    )
