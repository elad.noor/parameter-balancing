"""unit test for posterior sampling."""
# The MIT License (MIT)
#
# Copyright (c) 2022 Weizmann Institute of Science
# Copyright (c) 2022 INRAE
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import numpy as np
import pytest
from path import Path

from pbalancing.sampler import PosteriorSampler


@pytest.mark.parametrize(
    "path,",
    [
        "ecoli_noor_2016_glycolysis_kinetic_posterior_matrices",
        # "hynne_example_posterior_matrices",
        "pb_ecoli_noor_2016_glycolysis_metabolic_posterior_matrices",
        "pb_ecoli_noor_2016_glycolysis_posterior_matrices",
    ],
)
def test_sampler(path):
    np.random.seed(2019)
    p = Path(__file__)
    p = p.parent.parent.parent / "hopsy_demo" / path
    assert p.exists()
    s = PosteriorSampler(path=p, step_size=1.0)
    acceptance_rates, samples, rhat, runtime = s.sample()

    assert 1.0 <= rhat <= 1.1
