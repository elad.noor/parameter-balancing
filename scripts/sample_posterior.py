# The MIT License (MIT)
#
# Copyright (c) 2018 Timo Lubitz
# Copyright (c) 2022 Elad Noor, Weizmann Institute of Science
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import argparse
import logging

import h5py
from path import Path

from pbalancing import logger
from pbalancing.sampler import PosteriorSampler


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("output_path", help="Path for writing the HDF5 output file")
    parser.add_argument(
        "-d", "--debug", action="store_true", help="full application debug mode"
    )
    parser.add_argument(
        "-q", "--quiet", action="store_true", help="suppress all console output"
    )

    args = parser.parse_args()

    if args.debug:
        logger.setLevel(logging.DEBUG)
    elif args.quiet:
        logger.setLevel(logging.ERROR)
    else:
        logger.setLevel(logging.INFO)

    params_list = [
        {
            "path": "hopsy_demo/ecoli_noor_2016_glycolysis_kinetic_posterior_matrices",
            "step_size": 1.0,
        },
        {"path": "hopsy_demo/hynne_example_posterior_matrices", "step_size": 1.0},
        {
            "path": "hopsy_demo/pb_ecoli_noor_2016_glycolysis_metabolic_posterior_matrices",
            "step_size": 1.0,
        },
        {
            "path": "hopsy_demo/pb_ecoli_noor_2016_glycolysis_posterior_matrices",
            "step_size": 1.0,
        },
        # the next two models take quite some time to sample and also requires more samples
        # {
        #     "path": "hopsy_demo/pb_ecoli_noor_2016_metabolic_posterior_matrices",
        #     "step_size": 0.3,
        # },
        # {"path": "hopsy_demo/pb_ecoli_noor_2016_posterior_matrices", "step_size": 0.3},
    ]

    output_path = Path(args.output_path)
    if output_path.ext != ".hdf5":
        output_path += ".hdf5"

    with h5py.File(output_path, "w") as hdf5_file:
        for i, params in enumerate(params_list):
            logger.info(f"Working on example in {params['path']}")
            logger.debug(params)
            s = PosteriorSampler(**params)
            acceptance_rates, samples, rhat, runtime = s.sample()

            hdf5_grp = hdf5_file.create_group(f"dataset_{i}")
            hdf5_grp.attrs["path"] = params["path"]
            hdf5_grp.attrs["step_size"] = params["step_size"]
            hdf5_grp.create_dataset(
                "acceptance_rates",
                shape=(len(acceptance_rates)),
                data=acceptance_rates,
                dtype="float",
            )
            hdf5_grp.create_dataset(
                "samples", shape=samples.shape, dtype="float", data=samples
            )
            hdf5_grp.create_dataset(
                "runtime", shape=(1,), dtype="float", data=(runtime,)
            )
            hdf5_grp.create_dataset("rhat", shape=(1,), dtype="float", data=(rhat,))
